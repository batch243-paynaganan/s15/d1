// console.log("Hello World"):

// [Section] Syntax, Statements and Comments
// Statements in programming are instructions that we tell the computer to perform.

// Js statements usually end with semicolon(;).
// Semicolons are not required in JS, but we will use it to help us train to locate where the statement ends.
// A syntax in programming, it is the set of rules that we describes how statements must be constructed.
// All lines/blocks of codes should be written in specific manner to work. This is due to hopw these codes where initially programmed to function in certain maner.

	// Comments are part of the code that gets ignored by the language.
	// Comments are meant to describe the written code.
	
/*
	There are two types of comments:
	1. The single-line comment denoted by two forward slashes
	2. Multi-line comment denoted by forward slash and asterisk
*/

// [Section] Variables

// Variables are used to contain data.
// Any information that is use by an application is stored in what we call the "memory."
// When we create variables, certain portions of a device's memory is given a name that we call variables.

// Using variables, this makes it easier for us to associate information stored in our devices to actual "names" about information.

// Declaring Variables
// Declaring Variables - it tells our devices that a variable name is created and is ready to store date
// DEclaring a variable without giving it a value will automatically assighn it with the vaue of undefined, meaning the variable's value was not defined
	// Syntax
		// let / const VariableName;
'use strict'

let myVariable = "Ada Lovelace";
// const myVariable = "Ada Lovelace";
// Console.log is useful for printing values of variables or certain results of code into the Google Chrome browser's console.
// Constant use of of this throughout developing an application will save is time and builds good habit in always checking for the output of our code.
console.log(myVariable);
/*
Guides in writing variables:
	1. Use the let keyword followed the variable name of your choice and use the assignment operator (=) to assign value.
	2. Variable names should start with lowercase character, use camelCase for multiple words.
	3. For constant variables, use the 'const' keyword.
		using let: we can change the value of the variable.
		using const: we cannot change the value of the variable.
	4. Variable names should be indicative (desccriptive) of the value being stored to avoid confusion
	*/

// Declare and initialize variables
// Initializing variables - the instance when a variable is given it's initial /starting value
	// Syntax
		// let/const variableName = value;

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice)

	// In the context of certain applications, some variables/information are constant and should not be changed.
	// In this example, the interest rate for a loan savings account or a mortgage must not be changed due to real world concerns.

const interest = 3.539;
console.log(interest);

// Reassigning variable values
// Reassigning variable values means changing it's initial or previous value into another value.
	// Syntax
		// variableName = newValue;

productName = "laptop";
console.log(productName);
console.log(productName);

// let productName = "laptop";
// console.log(productName)

	// let variable cannot be re-declare within its scope.

// values of constants cannot be changed and will simply return an error

// interest = 4.489;
// console.log(interest)

// Reassigning variables and initializing variables


let supplier;
// initialization
supplier = "Zuitt Store";
// reassignment
supplier = "Zuitt Merch";

const name = "George Alfred Cabaccang";

// name = "George Alfred Cabaccang";

// var vs let/const
	// some of you may wonder why we use let/const keyword in declaring a variable but when we search online, we usually see var.
	// var is also used in declaring variable but var is an EcmaScript1 feature  [ES1 (JavaScript 1997)]
	// let/const was introduced as a neew feature in ES6 (2016)

	// What makes let/const different from var

a=5;
console.log(a)
var a;

// b=6;
// console.log(b)
// let b;

	// let/const local/global scope
	// scope essentially means where these variables are available for use
	// let/const are block scoped
	// A block is a chunk of code bounded by curly braces {}. A block in curly braces or anything within the curly braces is a block.

	let outerVariable = "hello from the other side";

	// {
	// 	let innerVariable = "hello from the block";
	// 	console.log(innerVariable);
	// 	console.log(outerVariable);
	// }

	{
		let innerVariable = "hello from the second block";
		console.log(innerVariable);
		let outerVariable = "hello from the second block";
	}

	console.log(outerVariable);
	// console.log(innerVariable);

	// Multiple variable declarations
	// Multiple variable can be declared in one line

let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);
console.log(productCode);
console.log(productBrand);

// const let = "hello";
// console.log(let);

// [Sections ] Data Types
// Strings - are series of characters that create a word, phrase, sentence or anything related to creating text.
// Strings in JS can be written using either single (') and a double quote (").
// In other programming languages, only the double can be used for creating strings

let country = "Philippines";
let province = 'Metro Manila';

// Concatenate
// Multiple string values can be combined to create a single string string using the "+" symbol

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// The escape characters (\) in strings in combination with other characters can produce different effects.
let mailAddress = "Metro Manila\nPhilippines"
console.log(mailAddress);

let message = "John's employees went home early";

message = 'John\'s employees went home early';

console.log(message);


// numbers
// integers
let count = "26";
let headcount = 26;
console.log(count);
console.log(headcount);

// decimal numbers / fraction
let grade = 98.7;
console.log(grade);

// exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is " + grade);

console.log(count + headcount);

// boolean
// 

let isMarried = false;
let inGoodConduct = true;
console.log(isMarried);
console.log(inGoodConduct);
console.log("isMarried: " + isMarried);

// arrays
// arrays are a special kind of data type that's used to store multiple values.
// arrays can store different data types but is normally use to store similar data types

	// similar data types
		// syntax
		// let/const array name = [elementA, elementB, elementC, . . .]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades[1]);
console.log(grades);

// different data types
// storing data types inside an array is not recommended because it will not make sense in the context of programming.
let details = ["John", "Smith", 32, true];
console.log(details);

// objects
// are another special kind of data type taht's used to mimic real world objects/items.
// they're used to create complex data that contains pieces of information that are relevant to each other.
	/*
		Syntax:
		let/const objectName = {
			propertyA: value;
			propertyB: value;
		}
	*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["09171234567", "8123 4567"],
	address:{
		houseNumber: '345',
		city: "Manila"
	}
};

console.log(person);
console.log(person.age);
console.log(person.contact[1]);
console.log(person.address.city);

// typeof operator is used to determine the type of data or the value of a variable.

console.log(typeof person);
// Note: array is a special type of object with methods and functions to manipulate it. We will discuss thie methods in later sessions. (S22 JavaScript Array Manipulation).
console.log(typeof details);

/*
        Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object
*/

const anime = ['one piece', 'one punch man', 'attack on titans'];
// anime = ['kimetse no yaiba'];
console.log(anime);
anime[0]='kimetsu no yaiba';
console.log(anime);
anime[5]='dxd';
console.log(anime);
console.log(anime[4]);

// null
// it is used to intentionally expressed the absence of a value in a variable/initialization
// null simply means that a data type was assigned to a variable but it does not hold any value/

let spouse = null;
console.log(spouse);
// undefined
// represents the state of a variable that has been declared but without value

let fullName;
console.log(fullName);

// undefined vs null
// the difference between undefined and null is that for undefined, a variable was created but not provided a value.
// null means that a variable was created and was assigned a value that does not hold any value /amount.